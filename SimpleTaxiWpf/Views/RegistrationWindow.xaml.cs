﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }
        ClientService ClientService = new ClientService(new SimpleTaxiDbContext());

        private void btnRegisterNewClient_Click(object sender, RoutedEventArgs e)
        {
            Client client = new Client();
            Contact phone = new Contact();
            Contact email = new Contact();
            client.FirstName = textFirstName.Text;
            client.LastName = textLastName.Text;
            client.Code = ClientService.GenerateClientCode(client);
            client.Phone = textPhone.Text;
            phone.ContactType = SimpleTaxiConsole.Enums.ContactType.Phone;
            phone.ContactValue = textPhone.Text;
            email.ContactType = SimpleTaxiConsole.Enums.ContactType.Email;
            email.ContactValue = textEmail.Text;
            try
            {
                IClientService clientServices = new ClientService(new SimpleTaxiDbContext());
                clientServices.AddNewClient(client);
               int clientID = client.ClientId;
               ContactService contactService = new ContactService(new SimpleTaxiDbContext());
               contactService.SaveContact(clientID, phone);
               contactService.SaveContact(clientID, email);
                MessageBox.Show("Sucess! Your client code is "+ client.Code);
                Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong!");
            }
            
        }
    }
}
