﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Enums;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using SimpleTaxiWpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    public partial class ClientView : Page
    {
        private ClientWindowVM _vm;
        TripCalculator tripCalc = new TripCalculator();
        ITripService tripServices = new TripServices(new SimpleTaxiDbContext());
        public ClientView()
        {
            InitializeComponent();
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new ClientWindowVM();
            _vm.LoadData();
            this.DataContext = _vm;
        }
        TripServices ts = new TripServices(new SimpleTaxiDbContext());
        ClientService cs = new ClientService(new SimpleTaxiDbContext());
        CarServices carServ = new CarServices(new SimpleTaxiDbContext());

        private void btnRegisterNewClient_Click(object sender, RoutedEventArgs e)
        {
            RegistrationWindow rw = new RegistrationWindow();
            rw.Show();
        }
        private void btnCalculatePrice_Click(object sender, RoutedEventArgs e)
        {
            double price = GetPrice();
            string from = cmBoxFrom.Text;
            string to = cmBoxTo.Text;

            if (from != "" && to != "")
            {
                if (from == to)
                {
                    MessageBox.Show("You want to travel inside your district, fee " +
                        "will be " + price +" euro");
                }
                else
                {
                    double distance = tripCalc.GetDistBetweenStrings(cmBoxFrom.Text, cmBoxTo.Text);
                    MessageBox.Show("You are driving from " + from + " to " + to + "\n" +
                        "Trip distance is " + distance + " and price is "+price);
                }
            }
            else
            {
                MessageBox.Show("Please select where from and where to you want to go!");
            }
            lbPrice.Content = price + " euro";
        }

        public double GetPrice()
        {
            double price=0;
            string from = cmBoxFrom.Text;
            string to = cmBoxTo.Text;
            string carClass = comboClass.Text;
            double carCoeficent = 1;
           
            if (carClass == "Eco 0.6 eur/km")
            {
               
                carCoeficent = 0.6;
                price = 3;//fixed minimum price
            }
            else if (carClass == "Premium 1 eur/km")
            {
                carCoeficent = 1;
                price = 6;//fixed minimum price
            }
            else
            {
                carCoeficent = 1.5;
                price = 10;//fixed minimum price
            }

            if (from != "" && to != "")//first check if fields are not empty
            {
                if (from == to)
                {
                    return price;
                }
                else
                {
                    double distance = tripCalc.GetDistBetweenStrings(cmBoxFrom.Text, cmBoxTo.Text);

                    price = distance * carCoeficent;
                    return Math.Round(price,2);
                }
            }
            else return 0;
        }

        

        private void btnOrderCar_Click(object sender, RoutedEventArgs e)
        {
            
            Trip trip = new Trip();
            trip.From = cmBoxFrom.Text;
            trip.To = cmBoxTo.Text;
            trip.DateTime = DateTime.Now;
            trip.Distance = tripCalc.GetDistBetweenStrings(cmBoxFrom.Text, cmBoxTo.Text);
            trip.Price = GetPrice();
            string carClass = comboClass.Text;

            
                try
                {
                    Client tripClient;
                    if (clientCode.Text == null || clientCode.Text == "")
                    {
                        tripClient = cs.GetById(1);
                    }
                    else
                    {
                    tripClient = cs.GetByCode(clientCode.Text);
                    }
                    
                    tripClient.TripsMade++;
                    Car tripCar;
                   
                    if (carClass == "Eco 0.6 eur/km")
                    {
                         tripCar = carServ.GetByClassAndTripsMade(CarLevel.Economy);
                    }
                    else if (carClass == "Premium 1 eur/km")
                    {
                         tripCar = carServ.GetByClassAndTripsMade(CarLevel.Premium);
                    }
                    else
                    {
                        tripCar = carServ.GetByClassAndTripsMade(CarLevel.VIP);
                    }
                    tripCar.TripsMade++;
                    tripCar.CurrentDistrict = trip.To;
                    ts.SaveTripWithClientAndCar(trip, tripClient, tripCar);
                    cs.SaveClient(tripClient);
                    carServ.SaveCar(tripCar);
                    MessageBox.Show("Your car " + tripCar.MakeModel +" will arrive soon!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Something went wrong, please try again!");
                }                
            }           
        }
    }


