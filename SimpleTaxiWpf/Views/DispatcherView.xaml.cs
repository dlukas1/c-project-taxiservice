﻿using SimpleTaxiConsole.Services;
using SimpleTaxiWpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    /// <summary>
    /// Interaction logic for DispatcherView.xaml
    /// </summary>
    public partial class DispatcherView : Page
    {
        private DispatcherWindowVM _vm;

        public DispatcherView()
        {
            InitializeComponent();
            //Dynamically generate eventhandler
            //Starts when window is loaded
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new DispatcherWindowVM();
            _vm.LoadData();
            this.DataContext = _vm;
        }

        private void btnShowClients_Click(object sender, RoutedEventArgs e)
        {
            DispFrame.Content = new Views.AllClientsView();
        }

        private void btnShowCars_Click(object sender, RoutedEventArgs e)
        {
            DispFrame.Content = new Views.AllCarsView();
        }

        private void btnShowTrips_Click(object sender, RoutedEventArgs e)
        {
            DispFrame.Content = new Views.AllTripsView();
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RaportServices rs = new RaportServices(new SimpleTaxiConsole.SimpleTaxiDbContext());
                rs.SaveFullRaportToFile();
                MessageBox.Show("Raport saved to file!");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Unable to save " + ex);
            }
            
        }
    }
}