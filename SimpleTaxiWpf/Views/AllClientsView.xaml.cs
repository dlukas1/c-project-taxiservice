﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    /// <summary>
    /// Interaction logic for AllClientsView.xaml
    /// </summary>
    public partial class AllClientsView : Page
    {
        public AllClientsView()
        {
            InitializeComponent();
        }
        ClientService cs = new ClientService(new SimpleTaxiDbContext());
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cs.DeleteClientByCode(tbCode.Text);
                MessageBox.Show("Client deleted!");
            }
            catch (Exception)
            {
                MessageBox.Show("Operation failure");
            }
        }

        private void btnEditClient_Click(object sender, RoutedEventArgs e)
        {
            Client client = cs.GetByCode(tbCode.Text);
            client.FirstName = tbFirstName.Text;
            client.LastName = tbLastName.Text;
            client.Code = tbCode.Text;
            client.Phone = tbPhone.Text;
            cs.SaveClient(client);

        }
    }
}
