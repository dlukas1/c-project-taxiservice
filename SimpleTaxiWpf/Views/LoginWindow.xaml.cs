﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        
        public LoginWindow()
        {
            InitializeComponent();
        }
        public bool isAutorized = false;

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            //When login btn is pressed need to check credentials
            //and if correct tell MainWindow to show dispactcher view
            string adminLogin = "admin";
            string adminPassword = "admin";

            string login = txtLogin.Text;
            string password = txtPassword.Password;
            
            
            if (login == adminLogin && password == adminPassword)
            {
                isAutorized = true;
                Close();
            }
            else
            {
                MessageBox.Show(messageBoxText: "Incorrect login! Please try again!");
            }
       
        }
    }
}
