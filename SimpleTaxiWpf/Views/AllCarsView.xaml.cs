﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleTaxiWpf.Views
{
    /// <summary>
    /// Interaction logic for AllCarsView.xaml
    /// </summary>
    public partial class AllCarsView : Page
    {
        public AllCarsView()
        {
            InitializeComponent();
        }
        CarServices cs = new CarServices(new SimpleTaxiDbContext());
        private void btnAddCar_Click(object sender, RoutedEventArgs e)
        {
            Car newCar = new Car();
            newCar.RegNr = tbRegNr.Text;
            newCar.MakeModel = tbMakeModel.Text;

            if (int.TryParse(tbYearBuild.Text, out int year))
            { newCar.YearOfManufactory = year; }

            if (int.TryParse(tbFuel.Text , out int fuel))
            { newCar.FuelConsumptionPer100km = fuel; }


            if (int.TryParse(tbLevel.Text, out int level))
            {
                if (level == 2)
                {
                    newCar.Level = SimpleTaxiConsole.Enums.CarLevel.Premium;
                }
                else if (level == 3)
                {
                    newCar.Level = SimpleTaxiConsole.Enums.CarLevel.VIP;
                }
                else
                {
                    newCar.Level = SimpleTaxiConsole.Enums.CarLevel.Economy;
                }
            } 

            try
            {
                cs.SaveCar(newCar);
                MessageBox.Show("New car sucessfully saved!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service unavailable" + ex);
            }


        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cs.DeleteCarByRegNr(tbRegNr.Text);
                MessageBox.Show("Car deleted!");
            }
            catch (Exception)
            {
                MessageBox.Show("Operation failure");
            }
           
        }

       private void lboxText_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lboxText.SelectedIndex>-1)
            {
                  btnAddCar.Content="Save";  
            }
        }
    }
}
