﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SimpleTaxiWpf.ViewModels;
using SimpleTaxiConsole.Services;
using SimpleTaxiWpf.Views;
using System.Windows.Interop;

namespace SimpleTaxiWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DBinitializer dbi = new DBinitializer();
           string msg = dbi.initializeDB(); //dbi.initializeDB() returns msg if new DB is created or found existing
           MessageBox.Show(msg);
            //Dynamically generate eventhandler
            //Starts when window is loaded 
        }


        private void ClientView_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ClientView();
        }

        private void DispatcherView_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.ShowDialog(); // ShowDialog() allows to continue execution after lw is closed
            if (lw.isAutorized == true)
            {
                MainFrame.Content = new DispatcherView();
            }
        }
    }
    }

