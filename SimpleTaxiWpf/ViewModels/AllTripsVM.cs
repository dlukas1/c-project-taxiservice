﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiWpf.ViewModels
{
    public class AllTripsVM : BaseVM
    {
        private List<Trip> _trips;
        private ITripService _tripService;

        public List<Trip> Trips
        {
            get { return _trips; }
            private set
            {
                _trips = value;
                base.OnPropertyChanged("Trip");//gives mark to refresh UI
            }
        }

        //Пустой конструктор для инициализации List<Trip> _trips
        public AllTripsVM()
        {
            _trips = new List<Trip>();
            _tripService = new TripServices(new SimpleTaxiDbContext());
        }

        public void LoadData()
        {
            Trips = _tripService.GetAllTrips();
        }

        public void AddNewTrip(Trip t)
        {
            _tripService.SaveTrip(t);
            LoadData();//refresh view
        }


    }
}
