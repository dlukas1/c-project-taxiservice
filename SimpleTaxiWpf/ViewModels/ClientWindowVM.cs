﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiWpf.ViewModels
{
    public class ClientWindowVM : BaseVM
    {

        private List<Car> _cars;
        private List<Client> _clients;

        private IClientService _clientService;
        private ICarService _carService;


        public List<Client> Clients
        {
            get { return _clients; }
            private set
            {
                _clients = value;
                base.OnPropertyChanged("Client");//gives mark to refresh UI
            }
        }

        public List<Car> Cars
        {
            get { return _cars; }
            private set
            {
                _cars = value;
                base.OnPropertyChanged("Car");//gives mark to refresh UI
            }
        }



        //Empty constructor to initialize List<Trip> _trips
        public ClientWindowVM()
        {
            _cars = new List<Car>();
            _clients = new List<Client>();

            _clientService = new ClientService(new SimpleTaxiDbContext());
            _carService = new CarServices(new SimpleTaxiDbContext());
        }


        //Upon call method load info
        public void LoadData()
        {
            Clients = _clientService.GetAllClients();
            Cars = _carService.GetAllCars();
        }

        public void AddNewClient(Client c)
        {
            _clientService.SaveClient(c);
            LoadData();     //refresh view
        }

        public void EditSelectedClient(Client selected)
        {
            //TODO: edit selected
        }
    }
}

