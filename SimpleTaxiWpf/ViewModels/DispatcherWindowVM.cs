﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiWpf.ViewModels
{
    public class DispatcherWindowVM:BaseVM
    {
       
        private List<Car> _cars;
        private List<Client> _clients;
        private List<Trip> _trips;

        private IClientService _clientService;
        private ICarService _carService;
        private ITripService _tripService;
       

        public List<Client> Clients
        {
            get { return _clients; }
            private set
            {
                _clients = value;
                base.OnPropertyChanged("Client");//gives mark to refresh UI
            }
        }

        public List<Car> Cars
        {
            get { return _cars; }
            private set
            {
                _cars = value;
                base.OnPropertyChanged("Car");//gives mark to refresh UI
            }
        }

        public List<Trip> Trips
        {
            get { return _trips; }
            private set
            {
                _trips = value;
                base.OnPropertyChanged("Trip");//gives mark to refresh UI
            }
        }



        //Empty constructor to initialize List<Trip> _trips
        public DispatcherWindowVM()
        {
            _cars = new List<Car>();
            _clients = new List<Client>();
            _trips = new List<Trip>();

            _clientService = new ClientService(new SimpleTaxiDbContext());
            _carService = new CarServices(new SimpleTaxiDbContext());
            _tripService = new TripServices(new SimpleTaxiDbContext());
        }

        //Upon call method load info
        public void LoadData()
        {
            Clients = _clientService.GetAllClients();
            Cars = _carService.GetAllCars();
            Trips = _tripService.GetAllTrips();
        }

        public void AddNewClient(Client c)
        {
            _clientService.SaveClient(c);
            LoadData();     //refresh view
        }
    }
}