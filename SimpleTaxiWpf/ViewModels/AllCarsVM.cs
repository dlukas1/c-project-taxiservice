﻿using SimpleTaxiConsole;
using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiWpf.ViewModels
{
    class AllCarsVM : BaseVM
    {
        private List<Car> _cars;
        private ICarService _carService;

        public List<Car> Cars
        {
            get { return _cars; }
            private set
            {
                _cars = value;
                base.OnPropertyChanged("Cars");//gives mark to refresh UI
            }
        }

        //Empty constructor to initialize List<Car> _cars
        public AllCarsVM()
        {
            _cars = new List<Car>();
            _carService = new CarServices(new SimpleTaxiDbContext());
        }

        public void LoadData()
        {
            Cars = _carService.GetAllCars();
        }

        public void AddNewCar(Car c)
        {
            _carService.SaveCar(c);
            LoadData();//refresh view
        }


    }
}
