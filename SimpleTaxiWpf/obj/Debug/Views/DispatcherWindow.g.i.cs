﻿#pragma checksum "..\..\..\Views\DispatcherWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "47952728DA4F371C06093AE57292906D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SimpleTaxiWpf.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SimpleTaxiWpf.Views {
    
    
    /// <summary>
    /// DispatcherWindow
    /// </summary>
    public partial class DispatcherWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Views\DispatcherWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lboxText;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Views\DispatcherWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnShowClients;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Views\DispatcherWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnShowCars;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Views\DispatcherWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnShowTrips;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SimpleTaxiWpf;component/views/dispatcherwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\DispatcherWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lboxText = ((System.Windows.Controls.ListBox)(target));
            return;
            case 2:
            this.btnShowClients = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\Views\DispatcherWindow.xaml"
            this.btnShowClients.Click += new System.Windows.RoutedEventHandler(this.btnShowClients_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnShowCars = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\Views\DispatcherWindow.xaml"
            this.btnShowCars.Click += new System.Windows.RoutedEventHandler(this.btnShowCars_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnShowTrips = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\Views\DispatcherWindow.xaml"
            this.btnShowTrips.Click += new System.Windows.RoutedEventHandler(this.btnShowTrips_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

