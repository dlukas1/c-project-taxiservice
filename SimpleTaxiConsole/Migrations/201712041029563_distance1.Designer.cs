// <auto-generated />
namespace SimpleTaxiConsole.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class distance1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(distance1));
        
        string IMigrationMetadata.Id
        {
            get { return "201712041029563_distance1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
