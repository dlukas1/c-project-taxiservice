namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new_tables_contactTypes_Districts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactTypes",
                c => new
                    {
                        ContactType_Id = c.Int(nullable: false, identity: true),
                        ContactType = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.ContactType_Id);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        DistrictId = c.Int(nullable: false, identity: true),
                        districtName = c.String(nullable: false, maxLength: 40),
                        latX = c.Double(nullable: false),
                        lonY = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.DistrictId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Districts");
            DropTable("dbo.ContactTypes");
        }
    }
}
