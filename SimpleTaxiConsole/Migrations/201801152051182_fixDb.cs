namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixDb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contacts", "Client_ClientId", "dbo.Clients");
            DropIndex("dbo.Contacts", new[] { "Client_ClientId" });
            RenameColumn(table: "dbo.Contacts", name: "Client_ClientId", newName: "ClientId");
            AlterColumn("dbo.Contacts", "ClientId", c => c.Int(nullable: true));
            CreateIndex("dbo.Contacts", "ClientId");
            AddForeignKey("dbo.Contacts", "ClientId", "dbo.Clients", "ClientId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "ClientId", "dbo.Clients");
            DropIndex("dbo.Contacts", new[] { "ClientId" });
            AlterColumn("dbo.Contacts", "ClientId", c => c.Int());
            RenameColumn(table: "dbo.Contacts", name: "ClientId", newName: "Client_ClientId");
            CreateIndex("dbo.Contacts", "Client_ClientId");
            AddForeignKey("dbo.Contacts", "Client_ClientId", "dbo.Clients", "ClientId");
        }
    }
}
