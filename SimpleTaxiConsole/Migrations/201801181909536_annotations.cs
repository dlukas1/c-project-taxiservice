namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class annotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Trips", "From", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Trips", "To", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Contacts", "ContactValue", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Drivers", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Drivers", "Phone", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Drivers", "Phone", c => c.String());
            AlterColumn("dbo.Drivers", "Name", c => c.String());
            AlterColumn("dbo.Contacts", "ContactValue", c => c.String());
            AlterColumn("dbo.Trips", "To", c => c.String());
            AlterColumn("dbo.Trips", "From", c => c.String());
        }
    }
}
