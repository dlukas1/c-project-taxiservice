namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class someChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "YearOfManufactory", c => c.Int(nullable: false));
            AddColumn("dbo.Cars", "FuelConsumptionPer100km", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "FuelConsumptionPer100km");
            DropColumn("dbo.Cars", "YearOfManufactory");
        }
    }
}
