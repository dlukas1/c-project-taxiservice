namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignKeyFix : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Trips", name: "car_CarId", newName: "CarId_CarId");
            RenameColumn(table: "dbo.Trips", name: "client_ClientId", newName: "ClientId_ClientId");
            RenameIndex(table: "dbo.Trips", name: "IX_car_CarId", newName: "IX_CarId_CarId");
            RenameIndex(table: "dbo.Trips", name: "IX_client_ClientId", newName: "IX_ClientId_ClientId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Trips", name: "IX_ClientId_ClientId", newName: "IX_client_ClientId");
            RenameIndex(table: "dbo.Trips", name: "IX_CarId_CarId", newName: "IX_car_CarId");
            RenameColumn(table: "dbo.Trips", name: "ClientId_ClientId", newName: "client_ClientId");
            RenameColumn(table: "dbo.Trips", name: "CarId_CarId", newName: "car_CarId");
        }
    }
}
