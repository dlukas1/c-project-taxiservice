namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class driverAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        DriverId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                        CarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DriverId)
                .ForeignKey("dbo.Cars", t => t.CarId, cascadeDelete: true)
                .Index(t => t.CarId);
            
            AddColumn("dbo.Trips", "Driver_DriverId", c => c.Int());
            CreateIndex("dbo.Trips", "Driver_DriverId");
            AddForeignKey("dbo.Trips", "Driver_DriverId", "dbo.Drivers", "DriverId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trips", "Driver_DriverId", "dbo.Drivers");
            DropForeignKey("dbo.Drivers", "CarId", "dbo.Cars");
            DropIndex("dbo.Drivers", new[] { "CarId" });
            DropIndex("dbo.Trips", new[] { "Driver_DriverId" });
            DropColumn("dbo.Trips", "Driver_DriverId");
            DropTable("dbo.Drivers");
        }
    }
}
