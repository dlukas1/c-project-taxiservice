namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixInModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trips", "CarId_CarId", "dbo.Cars");
            DropForeignKey("dbo.Trips", "ClientId_ClientId", "dbo.Clients");
            DropIndex("dbo.Trips", new[] { "CarId_CarId" });
            DropIndex("dbo.Trips", new[] { "ClientId_ClientId" });
            DropIndex("dbo.Contacts", new[] { "client_ClientId" });
            RenameColumn(table: "dbo.Trips", name: "CarId_CarId", newName: "CarId");
            RenameColumn(table: "dbo.Trips", name: "ClientId_ClientId", newName: "ClientId");
            AddColumn("dbo.Clients", "Code", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.Contacts", "ContactType", c => c.Int(nullable: false));
            AlterColumn("dbo.Trips", "CarId", c => c.Int(nullable: false));
            AlterColumn("dbo.Trips", "ClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trips", "CarId");
            CreateIndex("dbo.Trips", "ClientId");
            CreateIndex("dbo.Contacts", "Client_ClientId");
            AddForeignKey("dbo.Trips", "CarId", "dbo.Cars", "CarId", cascadeDelete: true);
            AddForeignKey("dbo.Trips", "ClientId", "dbo.Clients", "ClientId", cascadeDelete: true);
            DropColumn("dbo.Contacts", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "Type", c => c.Int(nullable: false));
            DropForeignKey("dbo.Trips", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Trips", "CarId", "dbo.Cars");
            DropIndex("dbo.Contacts", new[] { "Client_ClientId" });
            DropIndex("dbo.Trips", new[] { "ClientId" });
            DropIndex("dbo.Trips", new[] { "CarId" });
            AlterColumn("dbo.Trips", "ClientId", c => c.Int());
            AlterColumn("dbo.Trips", "CarId", c => c.Int());
            DropColumn("dbo.Contacts", "ContactType");
            DropColumn("dbo.Clients", "Code");
            RenameColumn(table: "dbo.Trips", name: "ClientId", newName: "ClientId_ClientId");
            RenameColumn(table: "dbo.Trips", name: "CarId", newName: "CarId_CarId");
            CreateIndex("dbo.Contacts", "client_ClientId");
            CreateIndex("dbo.Trips", "ClientId_ClientId");
            CreateIndex("dbo.Trips", "CarId_CarId");
            AddForeignKey("dbo.Trips", "ClientId_ClientId", "dbo.Clients", "ClientId");
            AddForeignKey("dbo.Trips", "CarId_CarId", "dbo.Cars", "CarId");
        }
    }
}
