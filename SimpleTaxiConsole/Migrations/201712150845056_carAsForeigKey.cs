namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carAsForeigKey : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Trips", new[] { "Car_CarId" });
            DropIndex("dbo.Trips", new[] { "Client_ClientId" });
            CreateIndex("dbo.Trips", "car_CarId");
            CreateIndex("dbo.Trips", "client_ClientId");
            DropColumn("dbo.Trips", "CarRegNr");
            DropColumn("dbo.Trips", "ClientName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trips", "ClientName", c => c.String());
            AddColumn("dbo.Trips", "CarRegNr", c => c.String());
            DropIndex("dbo.Trips", new[] { "client_ClientId" });
            DropIndex("dbo.Trips", new[] { "car_CarId" });
            CreateIndex("dbo.Trips", "Client_ClientId");
            CreateIndex("dbo.Trips", "Car_CarId");
        }
    }
}
