namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clients", "Code", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clients", "Code", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
