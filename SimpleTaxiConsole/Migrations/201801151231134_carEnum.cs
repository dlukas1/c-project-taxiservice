namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carEnum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "CurrentDistrict", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "CurrentDistrict");
        }
    }
}
