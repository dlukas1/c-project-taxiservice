namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTripToCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "TripsMade", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "TripsMade");
        }
    }
}
