namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class distanceAndPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "Distance", c => c.Double(nullable: false));
            AddColumn("dbo.Trips", "Price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trips", "Price");
            DropColumn("dbo.Trips", "Distance");
        }
    }
}
