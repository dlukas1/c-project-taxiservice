namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedDateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "DateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trips", "DateTime");
        }
    }
}
