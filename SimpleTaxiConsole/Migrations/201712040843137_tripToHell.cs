namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tripToHell : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        CarId = c.Int(nullable: false, identity: true),
                        RegNr = c.String(nullable: false, maxLength: 100),
                        MakeModel = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.CarId);
            
            CreateTable(
                "dbo.Trips",
                c => new
                    {
                        TripId = c.Int(nullable: false, identity: true),
                        From = c.String(),
                        To = c.String(),
                        Car_CarId = c.Int(),
                        Client_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.TripId)
                .ForeignKey("dbo.Cars", t => t.Car_CarId)
                .ForeignKey("dbo.Clients", t => t.Client_ClientId)
                .Index(t => t.Car_CarId)
                .Index(t => t.Client_ClientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trips", "Client_ClientId", "dbo.Clients");
            DropForeignKey("dbo.Trips", "Car_CarId", "dbo.Cars");
            DropIndex("dbo.Trips", new[] { "Client_ClientId" });
            DropIndex("dbo.Trips", new[] { "Car_CarId" });
            DropTable("dbo.Trips");
            DropTable("dbo.Cars");
        }
    }
}
