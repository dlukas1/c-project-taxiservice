namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class distance1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ContactId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        ContactValue = c.String(),
                        client_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.ContactId)
                .ForeignKey("dbo.Clients", t => t.client_ClientId)
                .Index(t => t.client_ClientId);
            
            AddColumn("dbo.Cars", "Level", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "client_ClientId", "dbo.Clients");
            DropIndex("dbo.Contacts", new[] { "client_ClientId" });
            DropColumn("dbo.Clients", "status");
            DropColumn("dbo.Cars", "Level");
            DropTable("dbo.Contacts");
        }
    }
}
