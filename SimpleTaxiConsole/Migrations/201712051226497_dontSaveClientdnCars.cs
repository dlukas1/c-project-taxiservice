namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dontSaveClientdnCars : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "CarRegNr", c => c.String());
            AddColumn("dbo.Trips", "ClientName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trips", "ClientName");
            DropColumn("dbo.Trips", "CarRegNr");
        }
    }
}
