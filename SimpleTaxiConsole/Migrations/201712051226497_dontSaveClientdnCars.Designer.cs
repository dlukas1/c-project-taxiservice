// <auto-generated />
namespace SimpleTaxiConsole.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class dontSaveClientdnCars : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(dontSaveClientdnCars));
        
        string IMigrationMetadata.Id
        {
            get { return "201712051226497_dontSaveClientdnCars"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
