namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reportAdd_GetAll : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Raports",
                c => new
                    {
                        RaportId = c.Int(nullable: false, identity: true),
                        RaportDate = c.DateTime(nullable: false),
                        TripsTotal = c.Int(nullable: false),
                        MoneyEarned = c.Double(nullable: false),
                        FuelBurned = c.Double(nullable: false),
                        DailyRevenue = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.RaportId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Raports");
        }
    }
}
