namespace SimpleTaxiConsole.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rollBackNoDriver : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Drivers", "CarId", "dbo.Cars");
            DropForeignKey("dbo.Trips", "Driver_DriverId", "dbo.Drivers");
            DropIndex("dbo.Trips", new[] { "Driver_DriverId" });
            DropIndex("dbo.Drivers", new[] { "CarId" });
            DropColumn("dbo.Trips", "Driver_DriverId");
            DropTable("dbo.Drivers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        DriverId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(maxLength: 100),
                        CarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DriverId);
            
            AddColumn("dbo.Trips", "Driver_DriverId", c => c.Int());
            CreateIndex("dbo.Drivers", "CarId");
            CreateIndex("dbo.Trips", "Driver_DriverId");
            AddForeignKey("dbo.Trips", "Driver_DriverId", "dbo.Drivers", "DriverId");
            AddForeignKey("dbo.Drivers", "CarId", "dbo.Cars", "CarId", cascadeDelete: true);
        }
    }
}
