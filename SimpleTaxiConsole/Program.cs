﻿using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ITripService tripServices = new TripServices(new SimpleTaxiDbContext());
            IClientService clientServices = new ClientService(new SimpleTaxiDbContext());
            ICarService carServices = new CarServices(new SimpleTaxiDbContext());

            // var car = carServices.GetByRegNr("666ZBZ");
            //carServices.AddTrip("666ZBZ");
            // carServices.AddTrip("666ZBZ");
            // carServices.AddTrip("666ZBZ");
            // carServices.AddTrip("666ZBZ");

            // var client = clientServices.GetByName("Samanta Fox");
           
            Car car = new Car()
            {

                RegNr = "666BMW",
                MakeModel = "BMW X6",
                YearOfManufactory = 2017,
                FuelConsumptionPer100km = 13
            };
           
            
            Client client = new Client()
            {

                FirstName = "Jon",
                LastName = "Bon Jovi",
                Phone = "959595959"
            };
            carServices.SaveCar(car);
            clientServices.SaveClient(client);

            Trip trip = new Trip()
            {
                DateTime = DateTime.Now,
                ClientId = client.ClientId,
                CarId = car.CarId,
                From = "Heaven",
                To = "Hell",
                
            };
           
             //carServices.SaveCar(car);
             //clientServices.SaveClient(client);
            /*
            var clients = clientServices.GetAllClients();
            foreach (var c in clients)
            {
                Console.WriteLine("id {0} name {1} {2} code {3}", c.ClientId, c.FirstName, c.LastName, c.Code);
            }
            */

           tripServices.SaveTrip(trip);
             
            
            var trips = tripServices.GetAllTrips();
            
            foreach (var tr in trips)
            {
                Console.WriteLine(tr.Distance + " " + tr.From+" " +tr.To + " "+tr.DateTime +" "+tr.ClientId);
            }
            
            Console.ReadLine();
            
        }
    }
}
