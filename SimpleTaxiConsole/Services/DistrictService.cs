﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class DistrictService : BaseService
    {
        public DistrictService(SimpleTaxiDbContext db) : base(db){}
        DistrictList districtList = new DistrictList();

        public void LoadBasicDistricts()
        {
            foreach (var dis in districtList.GetBasicDistricts())
            {
                DataContext.Districts.Add(dis);
                DataContext.SaveChanges();
            }
        }

        public void LoadNewDistrict(District d)
        {
                DataContext.Districts.Add(d);
            DataContext.SaveChanges();
        }
    }
}
