﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class ContactTypesService : BaseService
    {
        public ContactTypesService(SimpleTaxiDbContext db) : base(db){}

        public void AddContactType (ContactTypes type)
        {
            DataContext.ContactTypes.Add(type);
            DataContext.SaveChanges();
        }
    }
}
