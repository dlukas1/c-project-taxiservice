﻿using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class ClientService : BaseService, IClientService
    {
        public ClientService(SimpleTaxiDbContext db) : base(db){}

        public Client AddNewClient(Client client) {
            DataContext.Clients.Add(client);

            DataContext.SaveChanges();
            return client;
        }


        //If this client exist - update his data, if not - create new client
        public Client SaveClient(Client client)
        {

            var dbClient = this.GetByCode(client.Code);
            var isNew = dbClient == null;
            var newClient = isNew ? new Client() : dbClient;

            newClient.FirstName = client.FirstName;
            newClient.LastName = client.LastName;
            newClient.Phone = client.Phone;
            newClient.Code = client.Code;

            if (isNew) DataContext.Clients.Add(client);

            DataContext.SaveChanges();
            return newClient;
        }

        public void DeleteClientByCode(string code)
        {
            var clientToDelete = GetByCode(code);
            DataContext.Clients.Remove(clientToDelete);
            DataContext.SaveChanges();
        }

        public Client GetById (int id)
        {
                return DataContext.Clients.Where(c => c.ClientId == id).SingleOrDefault();
        }

        public List<Client> GetByName (string name)
        {
            return DataContext.Clients.Where(c => c.LastName == name || c.FirstName == name).ToList();
        }

        public List<Client> GetAllClients()
        {
                //sort first by name then by status and return as list
                return DataContext.Clients.OrderBy(c => c.ClientId).ToList();
        }

        public Client GetByCode(string code)
        {
            return DataContext.Clients.Where(c => c.Code == code).FirstOrDefault();
        }


        //basic code is generated from 3 first char of name & surname i.e. Mikkey Mouse => mikmou
        public string GenerateClientCode(Client client)
        {
            //if client is new and do not have a code

                string code = "";
                if (client.FirstName.Length > 2 && client.LastName.Length > 2)
                {
                    string sub1 = client.FirstName.Substring(0, 3);
                    string sub2 = client.LastName.Substring(0, 3);
                    //put strings together
                    code = string.Concat(sub1, sub2).ToLower();
                }
                else
                {
                    code = string.Concat(client.FirstName, client.LastName).ToLower();
                }
            if (CheckIfCodeUnique(code) == true)
            {
                return code;
            }
            else
            {
                do
                {
                  code =  GenerateCustomCode(code);
                } while (CheckIfCodeUnique(code) == false); 
            }   
                return code;
        }

        public bool CheckIfCodeUnique(string code)
        {
            return (GetByCode(code) == null) ? true : false;
        }


        public string GenerateCustomCode (string oldCode)
        {
            if (oldCode.Length == 6)//if first match
            {
                return string.Concat(oldCode, "1").ToLower();
            }
            else
            {
                int oldIndex= Int32.Parse(oldCode.Substring(6));
                oldCode = oldCode.Substring(0, 6);
                int newIndex = oldIndex + 1;
                
                string newCode = string.Concat(oldCode, newIndex).ToLower();
                
                    return newCode;
                } 
            }
        }
    }

