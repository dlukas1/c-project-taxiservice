﻿using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class DBinitializer
    {
        //Check if there are items in DB, ig not - create sample car, client and trip
        public string initializeDB()
        {
            CarServices cs = new CarServices(new SimpleTaxiDbContext());
            List<Car> cars = cs.GetAllCars();
            ClientService clientService = new ClientService(new SimpleTaxiDbContext());
            List<Client> clients = clientService.GetAllClients();
            ContactTypesService contactTypesServ = new ContactTypesService(new SimpleTaxiDbContext());
            DistrictService districtService = new DistrictService(new SimpleTaxiDbContext());

            if (!cars.Any() || !clients.Any()) //if cars or clients lists are empty 
            { 
                ITripService tripServices = new TripServices(new SimpleTaxiDbContext());
                IClientService clientServices = new ClientService(new SimpleTaxiDbContext());
                ICarService carServices = new CarServices(new SimpleTaxiDbContext());
                Car car2 = new Car()
                {
                    RegNr = "111AAA",
                    MakeModel = "Mercedes S600",
                    YearOfManufactory = 2017,
                    FuelConsumptionPer100km = 15,
                    Level = Enums.CarLevel.Premium
                };
                Car car1 = new Car()
                {
                    RegNr = "222BBB",
                    MakeModel = "VW Passat",
                    YearOfManufactory = 2014,
                    FuelConsumptionPer100km = 10,
                    Level = Enums.CarLevel.Economy
                };
                Car car3 = new Car()
                {
                    RegNr = "333CCC",
                    MakeModel = "Bentley Bentyaga",
                    YearOfManufactory = 2017,
                    FuelConsumptionPer100km = 20,
                    Level = Enums.CarLevel.VIP
                };
                Client client = new Client()
                {
                    FirstName = "Test",
                    LastName = "Client",
                    Phone = "123456",
                    Code = "tester"
                };

                ContactTypes phone = new ContactTypes()
                {
                    ContactType = "Phone"
                };
                ContactTypes email = new ContactTypes()
                {
                    ContactType = "Email"
                };

                carServices.SaveCar(car1);
                carServices.SaveCar(car2);
                carServices.SaveCar(car3);
                clientServices.SaveClient(client);
                contactTypesServ.AddContactType(phone);
                contactTypesServ.AddContactType(email);
                districtService.LoadBasicDistricts();



                return "DataBase initialized with sample data";
            }

            return "DataBase found";

        }
       
    }
}
