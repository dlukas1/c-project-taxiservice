﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class TripCalculator
    {
        //Calculates distance between two points
        public double CalculateDisatnce(District start, District finish)
        {
            double latXdifference = calcDifference(start.latX, finish.latX);
            double lonYdifference = calcDifference(start.lonY, finish.lonY);

            //a^2+b^2=c^2
            return Math.Sqrt(Math.Pow(latXdifference, 2) + Math.Pow(lonYdifference, 2));
        }

        public double GetDistBetweenStrings(string sFrom, string sTo)
        {
            DistrictList dl = new DistrictList();
            District start = dl.GetDistrict(sFrom);
            District stop = dl.GetDistrict(sTo);
            return Math.Round(CalculateDisatnce(start, stop), 2);
        }

        public double calcDifference(double a, double b)
        {
            //calc difference between two numbers
            return Math.Abs(a - b);
        }
    }
}

