﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class RaportServices : BaseService
    {
        public RaportServices(SimpleTaxiDbContext db) : base(db){}


        CarServices carServices = new CarServices(new SimpleTaxiDbContext());
        public void AddRaport(Trip trip)
        {

            var dbRaport = this.ShowRaportByDate(trip.DateTime.Date); //ask for raport of this date
            var IsNewDay = dbRaport == null; 
            var r = IsNewDay ? new Raport() : dbRaport;

            r.RaportDate = trip.DateTime.Date;
            r.TripsTotal++;
            r.MoneyEarned += trip.Price;
            var car = carServices.GetCarById(trip.CarId);//ask for this car in DB - later change to ask for exaxt field
            double carFuelCons = car.FuelConsumptionPer100km; //cons/100km
            //x = carFuel*distance/100
            double tripFuel = Math.Round(trip.Distance * carFuelCons / 100, 2);//cons on trip
            r.FuelBurned += Math.Round(tripFuel, 2);
            r.DailyRevenue += Math.Round((trip.Price - tripFuel * 1.33) * 0.5);

            try
            {
                if (IsNewDay) DataContext.Raports.Add(r);
                DataContext.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Raport> ShowAllRaports()
        {
            return DataContext.Raports.OrderBy(r => r.RaportDate).ToList();
        }

        public void SaveFullRaportToFile()
        {
            List<Raport> raports = ShowAllRaports();
            StreamWriter writer = new StreamWriter("FullTaxiRaport.txt");//The second argument (true) specifies an append operation

            foreach (var rap in raports)
            {
                writer.WriteLine("Date: {0:d}, TripsTotal: {1} trips, Brutto income: {2} eur, Fuel: {3} l, Revenue: {4} eur", rap.RaportDate.Date, rap.TripsTotal, rap.MoneyEarned, rap.FuelBurned, rap.DailyRevenue);
            }
            writer.Close();
        }

        public Raport ShowRaportByDate(DateTime date)
        {
            return DataContext.Raports.Where(r => r.RaportDate == date).FirstOrDefault();
        }
    }
}
