﻿using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Services;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class TripServices : BaseService, ITripService
    {
        public TripServices(SimpleTaxiDbContext db) : base(db){ }


        public void SaveTrip(Trip trip)
        {
            trip.CarId = 1;
            trip.ClientId = 1;
            DataContext.TripLog.Add(trip);
            DataContext.SaveChanges();
        }

        public void SaveTripWithClientAndCar(Trip trip, Client client, Car car)
        {
            trip.CarId = car.CarId;
            trip.ClientId = client.ClientId;

            DataContext.TripLog.Add(trip);
            DataContext.SaveChanges();

            RaportServices rs = new RaportServices(new SimpleTaxiDbContext());
            rs.AddRaport(trip);

        }

        public List<Trip> GetAllTrips()
        {          
                //sort by date and return as list
                return DataContext.TripLog.OrderBy(t => t.DateTime).ToList();

                //Sort by distance and return as list
                //return DataContext.TripLog.OrderBy(t => t.Distance).ToList();           
        }

        public Trip GetById(int id)
        {
            return DataContext.TripLog.Where(t => t.TripId == id).SingleOrDefault();
        }

    }
}
