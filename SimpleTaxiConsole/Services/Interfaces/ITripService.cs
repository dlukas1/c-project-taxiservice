﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services.Interfaces
{
    public interface ITripService
    {
        void SaveTrip(Trip trip);
        List<Trip> GetAllTrips();
        Trip GetById(int id);

    }
}
