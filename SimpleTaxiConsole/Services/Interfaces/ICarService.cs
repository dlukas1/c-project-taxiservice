﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services.Interfaces
{
    public interface ICarService
    {
        Car SaveCar(Car car);
        List<Car> GetAllCars();
        Car GetByRegNr(string regNr);
        Car GetCarById(int carId);
        int AddTrip(string regNr);

    }
}
