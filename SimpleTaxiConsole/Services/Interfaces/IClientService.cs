﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SimpleTaxiConsole.Domain;

namespace SimpleTaxiConsole.Services.Interfaces
{
    public interface IClientService
    {
        Client AddNewClient(Client client);
        Client SaveClient(Client client);
        Client GetById(int id);
        List<Client> GetByName(string name);
        Client GetByCode(string code);
        List<Client> GetAllClients();
        string GenerateClientCode(Client client);
        bool CheckIfCodeUnique(string code);
        string GenerateCustomCode(string oldCode);
    }
}
