﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class BaseService
    {
        protected SimpleTaxiDbContext DataContext;

        public BaseService (SimpleTaxiDbContext db)
        {
            DataContext = db;
        }
    }
}
