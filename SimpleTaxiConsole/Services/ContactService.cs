﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class ContactService : BaseService
    {
        public ContactService(SimpleTaxiDbContext db) : base(db)
        {
        }

        public void SaveContact( int clientId, Contact contact)
        {
            contact.ClientId = clientId;
            DataContext.Contacts.Add(contact);
            DataContext.SaveChanges();
        }

        public List<Contact> GetContacts(int clientId)
        {
            return DataContext.Contacts.Where(c => c.ClientId == clientId).ToList();
        }
    }
}
