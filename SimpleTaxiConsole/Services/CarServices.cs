﻿using SimpleTaxiConsole.Domain;
using SimpleTaxiConsole.Enums;
using SimpleTaxiConsole.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Services
{
    public class CarServices : BaseService, ICarService
    {
        public CarServices(SimpleTaxiDbContext db) : base(db){ }

        public Car SaveCar(Car car)
        {
            var dbCar = this.GetByRegNr(car.RegNr);
            var isNew = dbCar == null;

            var newCar = isNew ? new Car() : dbCar;

            newCar.RegNr = car.RegNr;
            newCar.MakeModel = car.MakeModel;
            newCar.YearOfManufactory = car.YearOfManufactory;
            newCar.Level = car.Level;
            newCar.FuelConsumptionPer100km = car.FuelConsumptionPer100km;

            if (isNew) DataContext.Cars.Add(car);

            DataContext.SaveChanges();
            Console.WriteLine("New Car {0} saved!", newCar.MakeModel);
            Console.ReadLine();
            return newCar;
        }
        
        public void DeleteCarByRegNr(string regNr)
        {
            try
            {
                var carToDelete = this.GetByRegNr(regNr);
                DataContext.Cars.Remove(carToDelete);
                DataContext.SaveChanges();
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Something went wrong with deleting "+e);
            }
            
        }

        

        public List<Car> GetAllCars()
        {
                //sort by trips made number
                return DataContext.Cars.OrderBy(c => c.TripsMade).ToList();
        }

        public Car GetByRegNr(string regNr)
        {
            return DataContext.Cars.Where(c => c.RegNr == regNr).SingleOrDefault();
        }

        public int AddTrip(string regNr)
        {
            Car car = DataContext.Cars.Where(c => c.RegNr == regNr).SingleOrDefault(); ;
            car.TripsMade++;
            SaveCar(car);
            return car.TripsMade;
        }

        public Car GetCarById(int carId)
        {
            return DataContext.Cars.Where(c => c.CarId == carId).SingleOrDefault();
        }

        public Car GetByClassAndTripsMade (CarLevel carLvl)
        {   //method returns car of desired level with least trips made
            List<Car> levelCars = DataContext.Cars.Where(c => c.Level == carLvl).ToList();
            List <Car> sortedCars = levelCars.OrderBy(o => o.TripsMade).ToList();
            return sortedCars.ElementAt(0);
        }
    }
}
