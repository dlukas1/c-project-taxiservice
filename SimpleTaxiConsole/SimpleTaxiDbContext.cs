﻿using SimpleTaxiConsole.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole
{
    public class SimpleTaxiDbContext : DbContext
    {
        public SimpleTaxiDbContext():base("name = SimpleTaxiDB") { }

        public DbSet <Client> Clients { get; set; }
        public DbSet <Car> Cars { get; set; }
        public DbSet <Trip> TripLog { get; set; }
        public DbSet <Contact> Contacts { get; set; }
        public DbSet <Raport> Raports { get; set; }
        public DbSet <ContactTypes> ContactTypes { get; set; }
        public DbSet <District> Districts { get; set; }
    }
}
