﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Enums
{
    public enum CarLevel
    {
        Economy = 1,
        Premium = 2,
        VIP = 3
    }
}
