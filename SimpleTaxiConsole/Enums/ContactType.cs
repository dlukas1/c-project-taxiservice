﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Enums
{
    public enum ContactType
    {
        Phone = 1,
        Email = 2
    }
}
