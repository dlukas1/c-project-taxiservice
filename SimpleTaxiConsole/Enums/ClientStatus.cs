﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Enums
{
    public enum ClientStatus
    {
        Silver = 1,
        Gold = 2,
        Platinum = 3
    }
}
