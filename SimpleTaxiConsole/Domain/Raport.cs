﻿using SimpleTaxiConsole.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    //Raport will show how many trips made, money earned and fuel burned on each day
    public class Raport
    {
        public int RaportId { get; set; }
        public DateTime RaportDate { get; set; }
        public int TripsTotal { get; set; }
        public double MoneyEarned { get; set; }
        public double FuelBurned { get; set; }
        public double DailyRevenue { get; set; }//from each trip driver gets 50% after deduction of fuel price
        public virtual List <Trip> DayTrips { get; set; }
       
    }
}
