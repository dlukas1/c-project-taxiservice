﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class Trip
    {
        [Required]
        public int TripId { get; set; }
        [Required]
        public int CarId { get; set; }
        public virtual Car Car { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        [MaxLength(100)]
        public string From { get; set; }
        [Required]
        [MaxLength(100)]
        public string To { get; set; }
        public double Distance { get; set; }
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        [Required]
        public double Price { get; set; }
        public virtual Raport Raport { get; set; }

       
    }
}
