﻿using SimpleTaxiConsole.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class Client
    {
        public int ClientId { get; set; }

      //  [Required]
      //  [MaxLength(20)]
        public string Code { get; set; } //Client code should be unique

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Phone { get; set; }

        public int TripsMade { get; set; }

        //depending on quantity of trips client can get statuss
        public ClientStatus Status { get; set; }

        //Client can have several contacts and trips
        public virtual List<Contact> Contacts { get; set; }
        public virtual List<Trip> Trips { get; set; }
    }
}
