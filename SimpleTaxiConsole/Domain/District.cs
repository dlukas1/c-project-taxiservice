﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{//"District" is a point in the middle of district with coordinates latX and lonY
    public class District
    {
        [Required]
        public int DistrictId { get; set; }
        [Required]
        [MaxLength(40)]
        public string districtName { get; set; }

        public double latX { get; set; }
        public double lonY { get; set; }

        public District(string districtName, double latX, double lonY)
        {
            this.districtName = districtName;
            this.latX = latX;
            this.lonY = lonY;
        }

        public District() { }
    }
}
