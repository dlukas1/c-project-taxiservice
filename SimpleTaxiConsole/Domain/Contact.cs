﻿using SimpleTaxiConsole.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class Contact
    {
        [Required]
        public int ContactId { get; set; }
        [Required]
        public ContactType ContactType { get; set; }//enum 1 - phone, 2 - email
        [Required]
        [MaxLength(100)]
        public string ContactValue { get; set; }
        [Required]
        public int ClientId { get; set; }

        //Every contact connected to SINGLE person
        public virtual Client Client { get; set; }
    }
}
