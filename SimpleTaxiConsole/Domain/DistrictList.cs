﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class DistrictList
    {
        public District GetDistrict(string name)
        {
            //get District by name
            return districts.Single(s => s.districtName == name);
        }

        public List<District> districts = new List<District>
        {
            //Kesklinn is 0-point, all other's district coordinates
            //are estimately calculated in relation to 0-point
           new District ("Pirita", 8,8),
           new District ("Lasnamae", 8,1),
           new District ("Kesklinn", 0,0),
           new District ("Kopli", -4,3),
           new District ("Kristiine", -2,-2.5),
           new District ("Mustamae", -5,-4),
           new District ("Haabersti", -7,-2),
           new District ("Nomme", -7.5,-9)
        };

        public List<District> GetBasicDistricts()
        {
            return districts;
        }
    }
}

