﻿using SimpleTaxiConsole.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class Car
    {
        [Required]
        public int CarId { get; set; }

        [Required]
        [MaxLength(100)]
        public string RegNr { get; set; }
        [Required]
        [MaxLength(100)]
        public string MakeModel { get; set; }
        [Required]
        public int YearOfManufactory{ get; set; }
        public double FuelConsumptionPer100km { get; set; }
        public int TripsMade { get; set; }
        public CarLevel Level { get; set; } //enum economy, premium, VIP
        public string CurrentDistrict { get; set; }
        public virtual List <Trip> Trips { get; set; } //For every car should be separate table to write trips
    }
}
