﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaxiConsole.Domain
{
    public class ContactTypes
    {
        [Key]
        [Required]
        public int ContactType_Id { get; set; }
        [Required]
        [MaxLength(20)]
        public string ContactType { get; set; }
    }
}
