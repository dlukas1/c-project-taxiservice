# Simple Taxi Service

This project is made for educational purposes only. 

## Getting Started

When programm is run for first time it creates database and fill it with sample data to start with (client with code tester and 3 different cars).
User can register as a new client or use service incognito. 

## Current Features
* Register as a new client or drive incognito.
* Calculate price between different city districts and with different cars.
* Keep log on trips made as well as give full report since first run (stored @ SimpleTaxiService\SimpleTaxiWpf\bin\Debug\FullTaxiRaport.txt)
* Add, Edit or Remove cars and users

* ... and more to come


## Authors

**Dmitry Lukash**

**Tallinn 2018**